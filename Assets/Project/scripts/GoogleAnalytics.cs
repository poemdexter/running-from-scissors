﻿using UnityEngine;
using System.Collections;

public static class GoogleAnalytics
{
    public static void SendEvent(string name)
    {
        Analytics.gua.beginHit(GoogleUniversalAnalytics.HitType.Event);
        Analytics.gua.addEventCategory("gameplay");
        Analytics.gua.addEventAction(name);
        Analytics.gua.sendHit();
    }

    public static void SendEvent(string name, int value)
    {
        Analytics.gua.beginHit(GoogleUniversalAnalytics.HitType.Event);
        Analytics.gua.addEventCategory("gameplay");
        Analytics.gua.addEventAction(name);
        Analytics.gua.addEventValue(value);
        Analytics.gua.sendHit();
    }
}
