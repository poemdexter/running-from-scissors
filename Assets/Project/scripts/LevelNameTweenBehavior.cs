﻿using UnityEngine;
using System.Collections;

public class LevelNameTweenBehavior : MonoBehaviour
{
    private UILabel label;

    void Start()
    {
        label = GetComponent<UILabel>();
    }

    public void ResetAndRun(string levelName)
    {
        gameObject.SetActive(true);
        label.text = levelName;
    }

    public void LevelNameFinished()
    {
        gameObject.SetActive(false);
    }
}
