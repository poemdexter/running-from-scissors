﻿using UnityEngine;
using System.Collections;

public class TileBehavior : MonoBehaviour
{
    private bool on;
    public bool teleportTile;
    private SpriteRenderer render;
    public Sprite litSprite;
    public Sprite unlitSprite;
    private TilesManager manager;

    private void Start()
    {
        render = GetComponent<SpriteRenderer>();
        manager = GameObject.FindGameObjectWithTag("GameController").GetComponent<TilesManager>();
    }

    public int Touch()
    {
        if (teleportTile)
        {
            manager.TouchTeleport();
            return 0;
        }
        else
        {
            on = !on;
            if (on)
            {
                TurnOn();
                AudioController.Play("lighton");
            }
            else
            {
                TurnOff();
                AudioController.Play("lightoff");
            }
            return (on) ? -1 : 1;
        }
    }

    public void SetActiveState(bool isOn)
    {
        on = isOn;
    }

    private void TurnOn()
    {
        Debug.Log("Change Tile State: ON");
        render.sprite = litSprite;
        transform.localScale = new Vector3(.25f, .25f, 1f);
    }

    private void TurnOff()
    {
        Debug.Log("Change Tile State: OFF");
        render.sprite = unlitSprite;
        transform.localScale = Vector3.one;
    }
}