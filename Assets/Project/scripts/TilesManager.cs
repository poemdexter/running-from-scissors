﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using poemdexter.RFS;
using Prime31.GoKitLite;

public class TilesManager : MonoBehaviour
{
    public GameObject litTile;
    public GameObject unlitTile;
    public GameObject teleportTile;
    public GameObject barrierHorizontal;
    public GameObject barrierVertical;
    public int maxLevels = 6;
    private PlayerBehavior player;
    private LevelManager levelManager;
    private bool moving;
    public Level CurrentLevel { get; private set; }
    private int tilesLeft;
    private int movesLeft;
    private LevelNameTweenBehavior levelLabel;
    private UILabel movesLeftLabel;
    private FadeScreen fadeScreen;
    private FadeWhiteScreen fadeWhiteScreen;
    private Vector2 queueMove;
    private GooglePlay googlePlay;
    private TeleportBehavior teleport;

    private bool gameStarted;

    private float hOffset, vOffset;

    public enum TileType
    {
        Empty,
        Unlit,
        Lit,
        PlayerSpawn,
        Teleport
    }

    private void Start()
    {
        teleport = GetComponent<TeleportBehavior>();
        levelManager = GetComponent<LevelManager>();
        levelManager.LoadLevelFile();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBehavior>();
        levelLabel = GameObject.FindGameObjectWithTag("LevelName").GetComponent<LevelNameTweenBehavior>();
        movesLeftLabel = GameObject.FindGameObjectWithTag("MovesLeft").GetComponent<UILabel>();
        googlePlay = GameObject.FindGameObjectWithTag("GooglePlay").GetComponent<GooglePlay>();
        fadeScreen = Camera.main.GetComponent<FadeScreen>();
        fadeWhiteScreen = Camera.main.GetComponent<FadeWhiteScreen>();
    }

    private void InstantiateLevel(int levelNumber)
    {
        CurrentLevel = levelManager.GetLevel(levelNumber);
        tilesLeft = CurrentLevel.UnlitTiles;
        movesLeft = CurrentLevel.Moves;
        movesLeftLabel.text = "Moves Left: " + movesLeft;

        hOffset = GetTilePlacementOffset(CurrentLevel.Width);
        vOffset = GetTilePlacementOffset(CurrentLevel.Height);

        for (int i = 0; i < CurrentLevel.Width; i++)
        {
            for (int j = 0; j < CurrentLevel.Height; j++)
            {
                int tileID = CurrentLevel.Layout[j][i];
                GameObject tile = null;
                if (tileID == (int) TileType.Lit)
                {
                    tile =
                        Instantiate(litTile, new Vector2(1.1f*i - hOffset, -1.1f*j + vOffset), Quaternion.identity) as
                            GameObject;
                    tile.GetComponent<TileBehavior>().SetActiveState(true);
                }
                else if (tileID == (int) TileType.Unlit)
                {
                    tile =
                        Instantiate(unlitTile, new Vector2(1.1f*i - hOffset, -1.1f*j + vOffset), Quaternion.identity) as
                            GameObject;
                    tile.GetComponent<TileBehavior>().SetActiveState(false);
                }
                else if (tileID == (int) TileType.PlayerSpawn)
                {
                    player.SetGridPosition(i, j);
                    player.MoveToGridPosition(1.1f*i - hOffset, -1.1f*j + vOffset);
                    tile =
                        Instantiate(litTile, new Vector2(1.1f*i - hOffset, -1.1f*j + vOffset), Quaternion.identity) as
                            GameObject;
                    tile.GetComponent<TileBehavior>().SetActiveState(true);
                }
                else if (tileID == (int) TileType.Teleport)
                {
                    tile =
                        Instantiate(teleportTile, new Vector2(1.1f*i - hOffset, -1.1f*j + vOffset), Quaternion.identity)
                            as GameObject;
                    teleport.SetTeleport(1.1f*i - hOffset, -1.1f*j + vOffset, i, j);
                    tile.GetComponent<TileBehavior>().teleportTile = true;
                }

                if (tile != null)
                {
                    tile.transform.parent = transform;
                    tile.name = i + "|" + j;
                }
            }
        }

        if (CurrentLevel.Barriers != null)
        {
            InstantiateBarriers();
        }

        fadeScreen.FadeOff();
        levelLabel.ResetAndRun(CurrentLevel.Name);
        if (Global.SelectedLevel == 1)
        {
            GameObject.FindGameObjectWithTag("TutLabel1").GetComponent<UILabel>().enabled = true;
            GameObject.FindGameObjectWithTag("TutLabel2").GetComponent<UILabel>().enabled = true;
        }
    }

    public void TouchTeleport()
    {
        moving = true;
        AudioController.Play("teleport");
        GoKitLite.instance.scaleTo(player.transform, .6f, new Vector3(0, 1, 1), 0, GoKitLiteEasing.Quadratic.EaseIn)
            .setCompletionHandler(TpOutDone);
    }

    private void TpOutDone(Transform p)
    {
        Teleport otherTile = teleport.GetOtherTile(player.GetGridPosition());
        p.position = new Vector2(otherTile.WorldPositionX,
            otherTile.WorldPositionY + vOffset - GetTPOutDoneOffset() - .14f);
        player.SetGridPosition((int) otherTile.GridPositionX, (int) otherTile.GridPositionY);
        GoKitLite.instance.scaleTo(player.transform, .6f, Vector3.one, 0, GoKitLiteEasing.Quadratic.EaseOut);
        moving = false;
    }

    private float GetTPOutDoneOffset()
    {
        switch (CurrentLevel.Height)
        {
            case 3:
                return .55f;
            case 4:
                return 1.1f;
            case 5:
                return 1.65f;
        }
        return 0;
    }

    private void InstantiateBarriers()
    {
        float hOffset = GetTilePlacementOffset(CurrentLevel.Width);
        float vOffset = GetTilePlacementOffset(CurrentLevel.Height);
        Debug.Log("offsets: " + hOffset + "," + vOffset);

        foreach (int b in CurrentLevel.Barriers)
        {
            int pos = Mathf.FloorToInt(b/11);
            int col = Utils.Mod(b, 11);
            Debug.Log("barrier position: " + pos + "," + col);

            int extraOffset = (CurrentLevel.Width%2 == 0) ? 0 : 1;
            //   1   2   3   4    5
            // 6 x 7 x 8 x 9 x 10 x 11
            GameObject barrier;
            if (col < 5)
            {
                // on top
                barrier =
                    (GameObject)
                        Instantiate(barrierHorizontal,
                            new Vector2(1.1f*(col - CurrentLevel.Height) + hOffset, -1.1f*(pos - 1) + vOffset - .55f),
                            Quaternion.identity);
            }
            else
            {
                float c = (CurrentLevel.Width == 4) ? 5 : CurrentLevel.Width;
                if (CurrentLevel.Width == 2)
                {
                    barrier =
                        (GameObject)
                            Instantiate(barrierVertical,
                                new Vector2(1.1f*(col - 7 - extraOffset) + hOffset - .55f, -1.1f*(pos) + vOffset),
                                Quaternion.identity);
                }
                else
                {
                    barrier =
                        (GameObject)
                            Instantiate(barrierVertical,
                                new Vector2(1.1f*(col - (4 + c) - extraOffset) + hOffset - .55f, -1.1f*(pos) + vOffset),
                                Quaternion.identity);
                }
            }
            barrier.transform.parent = transform;
        }
    }

    private float GetTilePlacementOffset(int length)
    {
        return .55f*(length - 1);
    }

    private void Update()
    {
        if (!gameStarted)
        {
            gameStarted = true;
            InstantiateLevel(Global.SelectedLevel);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            HandleMove(Utils.LEFT);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            HandleMove(Utils.RIGHT);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            HandleMove(Utils.UP);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            HandleMove(Utils.DOWN);
        }
    }

    public void HandleMove(Vector2 dir)
    {
        if (dir != Vector2.zero)
        {
            // move normally
            if (!moving && !IsBlockedByBarrier(dir))
            {
                if (CanMove(dir))
                {
                    moving = true;
                    queueMove = Vector2.zero;
                    player.Jump(dir, false);
                }
                else if (CanPhase(dir))
                {
                    moving = true;
                    queueMove = Vector2.zero;
                    player.Jump(dir, true);
                }
            }
                // queue move
            else if (moving && !IsBlockedByBarrier(dir))
            {
                queueMove = dir;
            }
        }
    }

    private bool IsBlockedByBarrier(Vector2 dir)
    {
        //    1     2     3     4      5
        // 6  X  7  X  8  X  9  X  10  X  11
        //    12    13    14    15     16
        Vector2 pos = player.GetGridPosition();

        float up = 11*(int) pos.y + 1 + (int) pos.x;
        float down = 11*(int) pos.y + 1 + 11 + (int) pos.x;
        float left = 11*(int) pos.y + 6 + (int) pos.x;
        float right = 11*(int) pos.y + 7 + (int) pos.x;

        Debug.Log("Position Barrier Checks: " + up + "," + down + "," + left + "," + right);

        bool blocked = false;
        if (dir == Utils.UP)
        {
            blocked = CurrentLevel.BarrierExists(up);
        }
        else if (dir == Utils.DOWN)
        {
            blocked = CurrentLevel.BarrierExists(down);
        }
        else if (dir == Utils.LEFT)
        {
            blocked = CurrentLevel.BarrierExists(left);
        }
        else if (dir == Utils.RIGHT)
        {
            blocked = CurrentLevel.BarrierExists(right);
        }

        if (blocked)
        {
#if !UNITY_WP8
            if (!googlePlay.achievement2)
                googlePlay.gp_AchievementComplete(2);
#endif
            GoogleAnalytics.SendEvent("barrier hit");
            player.RejectMoveAnimation();
        }
        return blocked;
    }

    private bool CanMove(Vector2 dir)
    {
        Vector2 pos = player.GetGridPosition() + dir;

        // can we move like normal
        if (pos.x >= 0 && pos.x < CurrentLevel.Width && pos.y >= 0 && pos.y < CurrentLevel.Height)
        {
            if (CurrentLevel.Layout[(int) pos.y][(int) pos.x] != (int) TileType.Empty)
            {
                return true;
            }
        }
        return false;
    }

    private bool CanPhase(Vector2 dir)
    {
        Vector2 pos = player.GetGridPosition() + dir;
        if (dir == Utils.LEFT && pos.x - 1 < 0)
        {
            if (CurrentLevel.Layout[(int) pos.y][CurrentLevel.Width - 1] != (int) TileType.Empty)
            {
                return true;
            }
        }
        else if (dir == Utils.RIGHT && pos.x == CurrentLevel.Width)
        {
            if (CurrentLevel.Layout[(int) pos.y][0] != (int) TileType.Empty)
            {
                return true;
            }
        }
        else if (dir == Utils.UP && pos.y - 1 < 0)
        {
            if (CurrentLevel.Layout[CurrentLevel.Height - 1][(int) pos.x] != (int) TileType.Empty)
            {
                return true;
            }
        }
        else if (dir == Utils.DOWN && pos.y == CurrentLevel.Height)
        {
            if (CurrentLevel.Layout[0][(int) pos.x] != (int) TileType.Empty)
            {
                return true;
            }
        }
        return false;
    }

    public void PlayerLanded(Vector2 dir)
    {
        tilesLeft += GetTileUnderPlayer().Touch();
        movesLeftLabel.text = "Moves Left: " + --movesLeft;
        StartCoroutine(DelayMoveAgain());
    }

    private IEnumerator DelayMoveAgain()
    {
        for (float timer = .2f; timer >= 0; timer -= Time.deltaTime)
        {
            yield return 0;
        }

        if (tilesLeft == 0)
        {
            WinLevel();
        }
        else if (movesLeft == 0)
        {
            LoseLevel();
        }
        else
        {
            moving = false;
            if (queueMove != Vector2.zero)
            {
                HandleMove(queueMove);
            }
        }
    }

    private TileBehavior GetTileUnderPlayer()
    {
        Vector2 pos = player.GetGridPosition();
        Debug.Log("Getting tile: " + pos.x + "|" + pos.y);
        return GameObject.Find((pos.x + "|" + pos.y)).GetComponent<TileBehavior>();
    }

    private void WinLevel()
    {
        moving = true;
        teleport.Reset();
#if !UNITY_WP8
        googlePlay.WinLevel(Global.SelectedLevel);
#endif
        if (Global.SelectedLevel == 1)
        {
            GameObject.FindGameObjectWithTag("TutLabel1").GetComponent<UILabel>().enabled = false;
            GameObject.FindGameObjectWithTag("TutLabel2").GetComponent<UILabel>().enabled = false;
        }

        if (Global.SelectedLevel == 25)
        {
            if (Global.CurrentLevel < Global.SelectedLevel + 1)
            {
                Global.CurrentLevel = Global.SelectedLevel + 1;
            }

            Global.SelectedLevel += 1;

            // big winner
            fadeWhiteScreen.FadeOn();
            GoKitLite.instance.positionTo(player.transform, 1.5f, player.transform.position + (Vector3.up*10f), 0,
                GoKitLiteEasing.Exponential.EaseIn);
            StartCoroutine(ShowEnding());
        }
        else
        {
            WinEffects();
            fadeScreen.FadeOn();
            StartCoroutine(FadeOut());

            if (Global.CurrentLevel < Global.SelectedLevel + 1)
            {
                Global.CurrentLevel = Global.SelectedLevel + 1;
            }

            Global.SelectedLevel += 1;
        }
    }

    private IEnumerator ShowEnding()
    {
        for (float timer = 1f; timer >= 0; timer -= Time.deltaTime)
        {
            yield return 0;
        }

        Application.LoadLevel("gameover");
    }


    public void LoseLevel()
    {
        moving = true;
        fadeScreen.FadeOn();
        StartCoroutine(FadeOut());
    }

    private void WinEffects()
    {
    }

    private IEnumerator FadeOut()
    {
        for (float timer = 1.3f; timer >= 0; timer -= Time.deltaTime)
        {
            yield return 0;
        }

        CleanLevel();
        InstantiateLevel(Global.SelectedLevel);
        moving = false;
    }

    private void CleanLevel()
    {
        var children = new List<GameObject>();
        foreach (Transform child in transform)
        {
            children.Add(child.gameObject);
        }
        children.ForEach(child => Destroy(child));
    }
}