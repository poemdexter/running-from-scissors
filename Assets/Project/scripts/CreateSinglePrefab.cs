﻿using UnityEngine;
using System.Collections;

public class CreateSinglePrefab : MonoBehaviour
{
    public GameObject googlePlayPrefab;
    public GameObject audioController;

    void Awake()
    {
        if (!GameObject.FindGameObjectWithTag("GooglePlay"))
            Instantiate(googlePlayPrefab);

        if (!GameObject.FindGameObjectWithTag("AudioController"))
            Instantiate(audioController);
    }
}
