﻿using UnityEngine;
using System.Collections.Generic;
using poemdexter.RFS;

public class LevelManager : MonoBehaviour
{
    private List<Level> levels = new List<Level>();

    public Level GetLevel(int level)
    {
        return levels[level - 1];
    }

    public void LoadLevelNames()
    {
        TextAsset dataAsset = (TextAsset)Resources.Load("levels", typeof(TextAsset));
        if (!dataAsset) Debug.LogError("missing levels.txt");

        List<object> levelsData = dataAsset.text.listFromJson();

        for (int i = 0; i < levelsData.Count; i++)
        {
            Dictionary<string, object> level = (Dictionary<string, object>)levelsData[i];
            string name = level["name"].ToString();
            levels.Add(new Level(name));
        }
    }

    public void LoadLevelFile()
    {
        TextAsset dataAsset = (TextAsset)Resources.Load("levels", typeof(TextAsset));
        if (!dataAsset) Debug.LogError("missing levels.txt");

        List<object> levelsData = dataAsset.text.listFromJson();

        for (int i = 0; i < levelsData.Count; i++)
        {
            Dictionary<string, object> level = (Dictionary<string, object>)levelsData[i];

            int number = int.Parse(level["level"].ToString());
            string name = level["name"].ToString();
            int unlitTiles = int.Parse(level["unlit_tiles"].ToString());
            int moves = int.Parse(level["moves"].ToString());
            int height = int.Parse(level["height"].ToString());
            int width = int.Parse(level["width"].ToString());
            List<object> layoutList = (List<object>)level["layout"];
            int[][] layout = ParseLayout(layoutList);

            if (!level.ContainsKey("barriers"))
            {
                levels.Add(new Level(number, name, unlitTiles, moves, height, width, layout, null));
            }
            else
            {
                List<object> barrierList = (List<object>)level["barriers"];
                List<int> barriers = ParseBarriers(barrierList);

                levels.Add(new Level(number, name, unlitTiles, moves, height, width, layout, barriers));
            }
        }
    }

    private List<int> ParseBarriers(List<object> barrierList)
    {
        List<int> barriers = new List<int>();
        foreach (object o in barrierList)
        {
            barriers.Add(int.Parse(o.ToString()));
        }
        return barriers;
    }

    private int[][] ParseLayout(List<object> layoutList)
    {
        List<int[]> rows = new List<int[]>();
        for (int i = 0; i < layoutList.Count; i++)
        {
            char[] rowTiles = layoutList[i].ToString().ToCharArray();
            List<int> row = new List<int>();
            foreach (char c in rowTiles)
            {
                row.Add(c - '0');
            }
            rows.Add(row.ToArray());
        }
        return rows.ToArray();
    }
}
