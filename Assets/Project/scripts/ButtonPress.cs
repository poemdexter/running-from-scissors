﻿using UnityEngine;
using System.Collections;

public class ButtonPress : MonoBehaviour
{
    private UISprite sprite;
    public string downName;
    public string upName;
    public bool isSliced;

    void Start()
    {
        sprite = GetComponent<UISprite>();
    }

    public void OnPress(bool isDown)
    {
        sprite.spriteName = (isDown) ? downName : upName;
        if (isSliced)
            sprite.type = UIBasicSprite.Type.Sliced;
        
        if (!isDown)
            AudioController.Play("button");

        Debug.Log(isDown);
    }
}
