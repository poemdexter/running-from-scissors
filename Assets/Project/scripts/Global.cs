﻿using poemdexter.RFS;
using UnityEngine;
using System.Collections;

public static class Global
{
    private static string cameFrom = "title";
    private static int cloudSaveLevel = -1;
    private static int playerPrefsLevel = -1;
    private static int currentLevel = -1;
    private static int selectedLevel = -1;

    public static int CloudSaveLevel
    {
        get { return cloudSaveLevel; }
        set
        {
            cloudSaveLevel = value;
            ResolveCurrentLevel();
        }
    }

    public static int PlayerPrefsLevel
    {
        get { return playerPrefsLevel; }
        set
        {
            playerPrefsLevel = value;
            ResolveCurrentLevel();
        }
    }

    public static int CurrentLevel
    {
        get { return currentLevel; }
        set
        {
            currentLevel = value;
            ResolveCurrentLevel();
        }
    }

    public static int SelectedLevel
    {
        get { return selectedLevel; }
        set { selectedLevel = value; }
    }

    public static string CameFrom
    {
        get { return cameFrom; }
        set { cameFrom = value; }
    }

    private static void ResolveCurrentLevel()
    {
        int current = currentLevel;
        currentLevel = (cloudSaveLevel > currentLevel) ? cloudSaveLevel : currentLevel;
        currentLevel = (playerPrefsLevel > currentLevel) ? playerPrefsLevel : currentLevel;
        if (currentLevel >= current)
        {
            PlayerPrefs.SetInt("Level", current);
            PlayerPrefs.Save();
        }
    }
}