﻿using UnityEngine;
using System.Collections.Generic;

namespace poemdexter.RFS
{
    public class Utils
    {
        public static readonly Vector2 UP = -Vector2.up;
        public static readonly Vector2 DOWN = Vector2.up;
        public static readonly Vector2 LEFT = -Vector2.right;
        public static readonly Vector2 RIGHT = Vector2.right;

        // C# doesn't have modulus so lets write our own
        public static int Mod(int a, int b)
        {
            int x = a % b;
            if (x < 0)
                x += b;
            return x;
        }

        public static Vector2 ConvertVec3ToVec2(Vector3 v3)
        {
            return new Vector2(v3.x, v3.y);
        }
    }
}