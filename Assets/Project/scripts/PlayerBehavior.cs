﻿using UnityEngine;
using System.Collections;
using Prime31.GoKitLite;
using poemdexter.RFS;

public class PlayerBehavior : MonoBehaviour
{
    public Vector3 playerVerticalOffset = new Vector3(0, .4f, 0);
    private Animator anim;
    private TilesManager game;
    private Vector2 jumpDirection;
    private Vector2 gridPosition;
    private float blinkDelay, currentTime;

    void Start()
    {
        anim = GetComponent<Animator>();
        blinkDelay = Random.Range(5f, 10f);
    }

    void Update()
    {
        if ((currentTime += Time.deltaTime) > blinkDelay)
        {
            anim.SetTrigger("Blink");
            currentTime = 0;
            blinkDelay = Random.Range(5f, 10f);
        }
    }

    public void RejectMoveAnimation()
    {
        anim.SetTrigger("Reject");
    }

    public void Jump(Vector2 dir, bool isPhase)
    {
        if (game == null)
            game = GameObject.FindGameObjectWithTag("GameController").GetComponent<TilesManager>();

        jumpDirection = dir;

        if (dir == Utils.LEFT)
            transform.localScale = new Vector3(-1f, 1f, 1f);
        if (dir == Utils.RIGHT)
            transform.localScale = Vector3.one;

        if (!isPhase)
        {
            gridPosition += new Vector2(dir.x, dir.y);

            if (dir == Utils.UP || dir == Utils.DOWN)
                dir = -dir;

            GoogleAnalytics.SendEvent("player jumped");
            anim.SetTrigger("Jump");
            GoKitLite.instance.positionTo(transform, .6f, Utils.ConvertVec3ToVec2(transform.position) + (dir * 1.1f), 0, GoKitLiteEasing.Quadratic.EaseInOut);
        }
        else
        {
            if (dir == Utils.LEFT) gridPosition = new Vector2(game.CurrentLevel.Width - 1, gridPosition.y);
            else if (dir == Utils.RIGHT) gridPosition = new Vector2(0, gridPosition.y);
            else if (dir == Utils.UP) gridPosition = new Vector2(gridPosition.x, game.CurrentLevel.Height - 1);
            else if (dir == Utils.DOWN) gridPosition = new Vector2(gridPosition.x, 0);

            if (dir == Utils.UP || dir == Utils.DOWN)
                dir = -dir;

            GoogleAnalytics.SendEvent("player jumped");
            anim.SetTrigger("Phase");
            GoKitLite.instance.positionTo(transform, .48f, Utils.ConvertVec3ToVec2(transform.position) + (dir * .55f), 0, GoKitLiteEasing.Quadratic.EaseInOut);
        }
    }

    public void FinishPhaseOut()
    {
        float hOffset = GetTilePlacementOffset(game.CurrentLevel.Width);
        float vOffset = GetTilePlacementOffset(game.CurrentLevel.Height);

        if (jumpDirection == Utils.LEFT)
            transform.position = new Vector2(hOffset + .55f, transform.position.y);
        else if (jumpDirection == Utils.RIGHT)
            transform.position = new Vector2(-hOffset - .55f, transform.position.y);
        else if (jumpDirection == Utils.UP)
            transform.position = new Vector2(transform.position.x, -vOffset - .55f + playerVerticalOffset.y);
        else if (jumpDirection == Utils.DOWN)
            transform.position = new Vector2(transform.position.x, vOffset + .55f + playerVerticalOffset.y);

        Vector2 d = (jumpDirection == Utils.UP || jumpDirection == Utils.DOWN) ? -jumpDirection : jumpDirection;
        GoKitLite.instance.positionTo(transform, .48f, Utils.ConvertVec3ToVec2(transform.position) + (d * .55f), 0, GoKitLiteEasing.Quadratic.EaseInOut);
    }

    public void FinishAnimation()
    {
        game.PlayerLanded(jumpDirection);
        Debug.Log("Finished Jump Animation");
    }

    public Vector2 GetGridPosition()
    {
        return gridPosition;
    }

    public void SetGridPosition(int i, int j)
    {
        Debug.Log("Setting Player Grid Position: " + i + "," + j);
        gridPosition = new Vector2(i, j);
    }

    public void MoveToGridPosition(float i, float j)
    {
        transform.position = new Vector3(i, j, 0) + playerVerticalOffset;
    }

    private float GetTilePlacementOffset(int length)
    {
        return .55f * (length - 1);
    }
}
