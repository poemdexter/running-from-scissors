﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;
using GooglePlayGames;

public class GooglePlay : MonoBehaviour
{
    public bool playGamesDebug = true;
    public string leaderboardID;
    public string[] achievementIDs;
    private bool authAndLoad;
    private bool loggedIn;
    public bool achievement2 = false;

#if !UNITY_WP8
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        PlayGamesPlatform.DebugLogEnabled = playGamesDebug;
        PlayGamesPlatform.Activate();

        if (PlayerPrefs.HasKey("Cheevo2"))
        {
            achievement2 = (PlayerPrefs.GetInt("Cheevo2") == 1) ? true : false;
        }
    }

    private void Update()
    {
        if (!authAndLoad)
        {
            authAndLoad = true;
            // we're logged in from previous session
            if (PlayerPrefs.HasKey("LoggedIn") && PlayerPrefs.GetInt("LoggedIn") == 1)
            {
                gp_Authenticate();
            }
        }
    }

    public void gp_Authenticate()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                GoogleAnalytics.SendEvent("google play auth success");
                loggedIn = true;
                PlayerPrefs.SetInt("LoggedIn", 1);
                PlayerPrefs.Save();
                gp_LeaderboardAdd(0); // throw 0 on leaderboard just so we're marked as played once
                GooglePlayCloudSave.GetInstance().LoadState();
            }
        });
    }

    public void gp_SignOut()
    {
        ((PlayGamesPlatform) Social.Active).SignOut();
        loggedIn = false;
        PlayerPrefs.SetInt("LoggedIn", 0);
        PlayerPrefs.Save();
    }

    public void gp_LeaderboardAdd(int score)
    {
        if (loggedIn)
        {
            Social.ReportScore(score, leaderboardID, (bool success) =>
            {
                // handle success or failure
            });
        }
    }

    public void gp_AchievementComplete(int cheevoNumber)
    {
        GoogleAnalytics.SendEvent("get achievement", cheevoNumber);
        PlayerPrefs.SetInt("Cheevo" + cheevoNumber, 1);
        PlayerPrefs.Save();
        if (loggedIn)
        {
            Social.ReportProgress(achievementIDs[cheevoNumber - 1], 100.0f, (bool success) =>
            {
                // handle success or failure
            });
        }
    }

    public void gp_ShowAchievements()
    {
        if (loggedIn)
        {
            Social.ShowAchievementsUI();
        }
    }

    public void gp_ShowLeaderboard()
    {
        if (loggedIn)
        {
            Social.ShowLeaderboardUI();
        }
    }

    public void WinLevel(int level)
    {
        GoogleAnalytics.SendEvent("level " + level + " beat");
        gp_LeaderboardAdd(level);

        switch (level)
        {
            case 2:
                gp_AchievementComplete(1); // Took A Risk achievement
                break;
            case 19:
                gp_AchievementComplete(3); // Take Me Away achievement
                break;
            case 23:
                gp_AchievementComplete(4); // Not Giving Up achievement
                break;
            case 25:
                gp_AchievementComplete(5); // Found the Light achievement
                break;
        }

        if (loggedIn)
        {
            GooglePlayCloudSave.GetInstance().SaveState();
        }
    }
#endif
}