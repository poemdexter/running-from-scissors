﻿using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using UnityEngine;

public class GooglePlayCloudSave : OnStateLoadedListener
{
#if !UNITY_WP8
    private int cheevo1;
    private int cheevo2;
    private int cheevo3;
    private int cheevo4;
    private int cheevo5;

    #region Singleton
    private static GooglePlayCloudSave instance;

    public static GooglePlayCloudSave GetInstance()
    {
        if (instance == null)
            instance = new GooglePlayCloudSave();
        return instance;
    }

    private GooglePlayCloudSave()
    {
        if (PlayerPrefs.HasKey("Level"))
        {
            int level = PlayerPrefs.GetInt("Level");
            Global.PlayerPrefsLevel = level;
        }
        else
        {
            PlayerPrefs.SetInt("Level", 1);
            Global.PlayerPrefsLevel = 1;
        } 

        if (PlayerPrefs.HasKey("Cheevo1"))
            cheevo1 = PlayerPrefs.GetInt("Cheevo1");
        else
            PlayerPrefs.SetInt("Cheevo1", 0);

        if (PlayerPrefs.HasKey("Cheevo2"))
            cheevo1 = PlayerPrefs.GetInt("Cheevo2");
        else
            PlayerPrefs.SetInt("Cheevo2", 0);

        if (PlayerPrefs.HasKey("Cheevo3"))
            cheevo1 = PlayerPrefs.GetInt("Cheevo3");
        else
            PlayerPrefs.SetInt("Cheevo3", 0);

        if (PlayerPrefs.HasKey("Cheevo4"))
            cheevo1 = PlayerPrefs.GetInt("Cheevo4");
        else
            PlayerPrefs.SetInt("Cheevo4", 0);

        if (PlayerPrefs.HasKey("Cheevo5"))
            cheevo1 = PlayerPrefs.GetInt("Cheevo5");
        else
            PlayerPrefs.SetInt("Cheevo5", 0);

        PlayerPrefs.Save();
    }
    #endregion

    public void SaveState()
    {
        // serialize your game state to a byte array:
        byte[] saveState = Serialize();
        int slot = 0; // slot number to use
        ((PlayGamesPlatform)Social.Active).UpdateState(slot, saveState, this);
    }

    public void OnStateSaved(bool success, int slot)
    {
        // handle success or failure
    }

    public void LoadState()
    {
        int slot = 0; // slot number to use
        ((PlayGamesPlatform)Social.Active).LoadState(slot, this);
    }

    public void OnStateLoaded(bool success, int slot, byte[] data)
    {
        if (success)
        {
            Deserialize(data);
        }
    }

    private byte[] Serialize()
    {
        byte[] save = new byte[6];
        save[0] = (byte)Global.CurrentLevel;
        save[1] = (byte)cheevo1;
        save[2] = (byte)cheevo2;
        save[3] = (byte)cheevo3;
        save[4] = (byte)cheevo4;
        save[5] = (byte)cheevo5;
        return save;
    }

    private void Deserialize(byte[] save)
    {
        if (save.Length == 6)
        {
            int lvl = (int)save[0];
            int ch1 = (int)save[1];
            int ch2 = (int)save[2];
            int ch3 = (int)save[3];
            int ch4 = (int)save[4];
            int ch5 = (int)save[5];

            Global.CloudSaveLevel = lvl;

            // Handle Conflicts
            if (lvl > Global.CurrentLevel)  // cloud > local
            {    
                PlayerPrefs.SetInt("Level", lvl);
            }

            if (ch1 > cheevo1)
            {
                cheevo1 = ch1;
                PlayerPrefs.SetInt("Cheevo1", cheevo1);
            }

            if (ch2 > cheevo2)
            {
                cheevo2 = ch2;
                PlayerPrefs.SetInt("Cheevo2", cheevo2);
            }

            if (ch3 > cheevo3)
            {
                cheevo3 = ch3;
                PlayerPrefs.SetInt("Cheevo3", cheevo3);
            }

            if (ch4 > cheevo4)
            {
                cheevo4 = ch4;
                PlayerPrefs.SetInt("Cheevo4", cheevo4);
            }

            if (ch5 > cheevo5)
            {
                cheevo5 = ch5;
                PlayerPrefs.SetInt("Cheevo5", cheevo5);
            }

            PlayerPrefs.Save();
        }
    }

    public byte[] OnStateConflict(int slot, byte[] local, byte[] server)
    {
        byte[] ret = new byte[6];
        ret[0] = (local[0] > server[0]) ? local[0] : server[0];
        ret[1] = (local[1] > server[1]) ? local[1] : server[1];
        ret[2] = (local[2] > server[2]) ? local[2] : server[2];
        ret[3] = (local[3] > server[3]) ? local[3] : server[3];
        ret[4] = (local[4] > server[4]) ? local[4] : server[4];
        ret[5] = (local[5] > server[5]) ? local[5] : server[5];
        return ret;
    }
#else
    public void OnStateLoaded(bool success, int slot, byte[] data)
    {
        
    }

    public byte[] OnStateConflict(int slot, byte[] localData, byte[] serverData)
    {
        return null;
    }

    public void OnStateSaved(bool success, int slot)
    {
        
    }
#endif
}
