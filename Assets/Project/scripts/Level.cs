﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace poemdexter.RFS
{
    public class Level
    {
        public int Number { get; private set; }
        public string Name { get; private set; }
        public int UnlitTiles { get; private set; }
        public int Moves { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }
        public int[][] Layout { get; private set; }
        public List<int> Barriers { get; set; }

        public Level(int number, string name, int unlitTiles, int moves, int height, int width, int[][] layout, List<int> barriers)
        {
            Number = number;
            Name = name;
            UnlitTiles = unlitTiles;
            Moves = moves;
            Height = height;
            Width = width;
            Layout = layout;
            Barriers = barriers;
        }

        public Level(string name)
        {
            Name = name;
        }

        public bool BarrierExists(float number)
        {
            return (Barriers != null && Barriers.Contains((int)number));
        }
    }
}
