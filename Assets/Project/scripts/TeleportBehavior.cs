﻿using UnityEngine;
using System.Collections;

public class TeleportBehavior : MonoBehaviour
{
    private Teleport tp1, tp2;

    public void SetTeleport(float wx, float wy, int gx, int gy)
    {
        if (tp1 == null || !tp1.IsSet)
        {
            tp1 = new Teleport(wx, wy, gx, gy);
        }
        else
        {
            tp2 = new Teleport(wx, wy, gx, gy);
        }
    }

    public bool IsTeleportTile(int x, int y)
    {
        return tp1.Is(x, y) || tp2.Is(x, y);
    }

    public Teleport GetOtherTile(Vector2 playerPosition)
    {
        if (tp1.GridPositionX == (int) playerPosition.x && tp1.GridPositionY == (int) playerPosition.y)
        {
            return tp2;
        }
        else
        {
            return tp1;
        }
    }

    public void Reset()
    {
        tp1 = null;
        tp2 = null;
    }
}

public class Teleport
{
    public bool IsSet { get; set; }
    public float WorldPositionX { get; set; }
    public float WorldPositionY { get; set; }
    public int GridPositionX { get; set; }
    public int GridPositionY { get; set; }

    public Teleport(float wx, float wy, int gx, int gy)
    {
        WorldPositionX = wx;
        WorldPositionY = wy;
        GridPositionX = gx;
        GridPositionY = gy;
        IsSet = true;
    }

    public bool Is(int x, int y)
    {
        return (GridPositionX == x && GridPositionY == y) ? true : false;
    }
}