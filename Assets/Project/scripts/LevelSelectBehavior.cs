﻿using System;
using System.Globalization;
using UnityEngine;
using System.Collections;

public class LevelSelectBehavior : MonoBehaviour
{
    private int currentLevel;
    private bool flipped = false;
    private Transform player;
    private LevelManager levels;
    private UILabel levelName;
    private bool wonGame = false;

    private void Start()
    {
        levels = GameObject.FindGameObjectWithTag("GooglePlay").GetComponent<LevelManager>();
        levelName = GameObject.FindGameObjectWithTag("LevelName").GetComponent<UILabel>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        levels.LoadLevelNames();

        if (PlayerPrefs.HasKey("Level"))
        {
            Global.PlayerPrefsLevel = PlayerPrefs.GetInt("Level");
        }

        if (Global.PlayerPrefsLevel <= 0)
        {
            Global.CurrentLevel = 1;
        }
        else
        {
            Global.CurrentLevel = Global.PlayerPrefsLevel;
        }

        if (Global.SelectedLevel <= 0)
        {
            Global.SelectedLevel = Global.CurrentLevel;
        }

        if (Global.SelectedLevel == 26 || Global.CurrentLevel == 26)
        {
            Global.SelectedLevel = 25;
            wonGame = true;
        }

        // DEBUG
        // Global.CurrentLevel = 25;
        // Global.SelectedLevel = 25;
        // wonGame = false;

        levelName.text = levels.GetLevel(Global.SelectedLevel).Name;
    }

    private void Update()
    {
        if (!flipped)
        {
            flipped = true;
            currentLevel = Global.CurrentLevel;

            Debug.Log("cl" + Global.CloudSaveLevel + " cr" + Global.CurrentLevel + " pp" + Global.PlayerPrefsLevel +
                      " se" + Global.SelectedLevel);

            Transform t = transform.FindChild(Global.SelectedLevel.ToString());
            player.localPosition = new Vector3(t.localPosition.x, t.localPosition.y + 25, 0);

            if (Global.CurrentLevel != 1)
            {
                // color everything we beat
                for (var i = 1; i < currentLevel; i++)
                {
                    Transform tile = transform.FindChild((i).ToString());
                    tile.GetComponent<UISprite>().spriteName = "solidLight";
                    tile.GetComponent<UISprite>().color = new Color(1, 1, 1, 1);
                    tile.GetComponent<UIButton>().normalSprite = "solidLight";
                    tile.GetComponent<UIButton>().defaultColor = new Color(1, 1, 1, 1);
                    tile.collider.enabled = true;
                }

                Debug.Log("wongame " + wonGame);
                if (wonGame)
                {
                    Transform tile = transform.FindChild("25");
                    tile.GetComponent<UISprite>().spriteName = "solidLight";
                    tile.GetComponent<UISprite>().color = new Color(1, 1, 1, 1);
                    tile.GetComponent<UIButton>().normalSprite = "solidLight";
                    tile.GetComponent<UIButton>().defaultColor = new Color(1, 1, 1, 1);
                    tile.collider.enabled = true;
                }
                else if (currentLevel <= 25)
                {
                    Transform tile = transform.FindChild((currentLevel).ToString());
                    tile.GetComponent<UISprite>().color = new Color(1, 1, 1, 1);
                    tile.GetComponent<UIButton>().defaultColor = new Color(1, 1, 1, 1);
                    tile.GetComponent<ButtonPress>().downName = "solidDark_down";
                    tile.GetComponent<ButtonPress>().upName = "solidDark";
                    tile.collider.enabled = true;
                }
            }
            else
            {
                Transform tile = transform.FindChild((1).ToString());
                tile.GetComponent<UISprite>().color = new Color(1, 1, 1, 1);
                tile.GetComponent<UIButton>().defaultColor = new Color(1, 1, 1, 1);
                tile.GetComponent<ButtonPress>().downName = "solidDark_down";
                tile.GetComponent<ButtonPress>().upName = "solidDark";
                tile.collider.enabled = true;
            }
        }
    }

    public void OnLevelTileClick(string tileName)
    {
        int s = Int32.Parse(tileName);
        if (s == Global.SelectedLevel)
        {
            AudioController.Play("button");
            Camera.main.GetComponent<FadeScreen>().FadeOn();
            StartCoroutine(WaitForFadeGame());
        }
        else
        {
            Global.SelectedLevel = s;
            levelName.text = levels.GetLevel(s).Name;
            Transform t = transform.FindChild(tileName);
            player.localPosition = new Vector3(t.localPosition.x, t.localPosition.y + 25, 0);
        }
    }

    private IEnumerator WaitForFadeGame()
    {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
        {
            yield return 0;
        }
        Application.LoadLevel("game");
    }
}