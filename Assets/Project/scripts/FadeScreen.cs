﻿using UnityEngine;
using System.Collections;

public class FadeScreen : MonoBehaviour
{
    public bool fading = false;
    public Texture2D fadeTexture;
    public float fadeSpeed = 0.4f;
    public float alpha = 0.0f;

    public void FadeOn()
    {
        fading = true;
    }

    public void FadeOff()
    {
        fading = false;
        alpha = 0.0f;
    }

    void OnGUI()
    {
        if (fading)
        {
            alpha += fadeSpeed * Time.deltaTime;

            // fade screen
            GUI.color = new Color(0, 0, 0, alpha);
            GUI.depth = 100;
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
        }
    }
}
