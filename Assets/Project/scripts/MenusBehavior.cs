﻿using UnityEngine;
using System.Collections;

public class MenusBehavior : MonoBehaviour
{
    GooglePlay googlePlay;

    private bool musicInit, sfxInit, optInit, optOut;
    private bool isMusicOn = true;
    private bool isSfxOn = true;

    UIToggle optButton, musicButton, sfxButton;

    void Awake()
    {
        var opt = GameObject.FindGameObjectWithTag("OptOutButton");
        var music = GameObject.FindGameObjectWithTag("MusicButton");
        var sfx = GameObject.FindGameObjectWithTag("SfxButton");

        if (PlayerPrefs.HasKey("OptOut"))
            optOut = (PlayerPrefs.GetInt("OptOut") == 1) ? true : false;
        else
        {
            PlayerPrefs.SetInt("OptOut", 0);
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.HasKey("Music"))
        {
            isMusicOn = (PlayerPrefs.GetInt("Music") == 1) ? true : false;
            Debug.Log("Music on is " + isMusicOn);
        }
        else
        {
            isMusicOn = true;
            Debug.Log("(nopref) Music on is " + isMusicOn);
            PlayerPrefs.SetInt("Music", 1);
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.HasKey("Sfx"))
        {
            isSfxOn = (PlayerPrefs.GetInt("Sfx") == 1) ? true : false;
            Debug.Log("Sfx on is " + isSfxOn);
        }
        else
        {
            isSfxOn = true;
            Debug.Log("(nopref) Sfx on is " + isSfxOn);
            PlayerPrefs.SetInt("Sfx", 1);
            PlayerPrefs.Save();
        }

        if (opt != null)
        {
            optButton = opt.GetComponent<UIToggle>();
            musicButton = music.GetComponent<UIToggle>();
            sfxButton = sfx.GetComponent<UIToggle>();

            optButton.value = optOut;
            musicButton.value = !isMusicOn;
            sfxButton.value = !isSfxOn;
        }
    }

    void Start()
    {
        googlePlay = GameObject.FindGameObjectWithTag("GooglePlay").GetComponent<GooglePlay>();

        if ((Global.CameFrom == "title" || Global.CameFrom == "levels") && Application.loadedLevelName == "menu")
        {
            GameObject.FindGameObjectWithTag("MainMenuButton").SetActive(false);
        }

        if (!isSfxOn)
        {
            AudioController.SetCategoryVolume("sfx", 0f);
        }
        if (!isMusicOn)
        {
            AudioController.SetCategoryVolume("music", 0f);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Global.CameFrom == "title")
            {
                Application.Quit();
            }
            else if (Global.CameFrom == "game")
            {
                LoadGame();
            }
            else if (Global.CameFrom == "levels")
            {
                LoadTitle();
            }
        } 
    }

    public void LoadGame()
    {
        AudioController.Play("button");
        Camera.main.GetComponent<FadeScreen>().FadeOn();
        StartCoroutine(WaitForFadeGame());
    }

    public void LoadLevels()
    {
        AudioController.Play("button");
        Camera.main.GetComponent<FadeScreen>().FadeOn();
        StartCoroutine(WaitForFadeLevels());
    }

    IEnumerator WaitForFadeGame()
    {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;
        Application.LoadLevel("game");
    }

    IEnumerator WaitForFadeLevels()
    {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;
        Application.LoadLevel("levels");
    }

    public void LoadMenu()
    {
        AudioController.Play("button");
        Camera.main.GetComponent<FadeScreen>().FadeOn();
        Global.CameFrom = Application.loadedLevelName;
        StartCoroutine(WaitForFadeMenu());
    }

    IEnumerator WaitForFadeMenu()
    {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;
        Application.LoadLevel("menu");
    }

    public void MenuClicked()
    {
        Debug.Log("from " + Global.CameFrom);
        if (Global.CameFrom == "title")
        {
            LoadTitle();
        }
        else if (Global.CameFrom == "game")
        {
            LoadGame();
        }
        else if (Global.CameFrom == "levels")
        {
            LoadLevels();
        }
    }

    public void LoadTitle()
    {
        AudioController.Play("button");
        Camera.main.GetComponent<FadeScreen>().FadeOn();
        StartCoroutine(WaitForFadeTitle());
    }

    IEnumerator WaitForFadeTitle()
    {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;
        Application.LoadLevel("title");
    }

    public void GooglePlayCheevos()
    {
        AudioController.Play("button");
        Debug.Log("Achievements...");
#if !UNITY_WP8
        googlePlay.gp_Authenticate();
        Social.ShowAchievementsUI();
#endif
    }

    public void GooglePlayLeader()
    {
        AudioController.Play("button");
        Debug.Log("Leaderboard...");
#if !UNITY_WP8
        googlePlay.gp_Authenticate();
        Social.ShowLeaderboardUI();
#endif
    }

    public void ToggleMusic()
    {
        if (musicInit)
        {
            AudioController.Play("button");
            isMusicOn = !isMusicOn;
            Debug.Log("Music ON: " + isMusicOn);
            AudioController.SetCategoryVolume("music", (isMusicOn) ? 1f : 0f);
            if (isMusicOn)
                AudioController.PlayMusic("DarkWayDown");
            else
                GoogleAnalytics.SendEvent("music toggled");
            PlayerPrefs.SetInt("Music", (isMusicOn) ? 1 : 0);
            PlayerPrefs.Save();
        }
        else
        {
            musicInit = true;
        }
    }

    public void ToggleSound()
    {
        if (sfxInit)
        {
            AudioController.Play("button");
            isSfxOn = !isSfxOn;
            Debug.Log("Sfx ON: " + isSfxOn);
            AudioController.SetCategoryVolume("sfx", (isSfxOn) ? 1f : 0f);
            PlayerPrefs.SetInt("Sfx", (isSfxOn) ? 1 : 0);
            PlayerPrefs.Save();
        }
        else
        {
            sfxInit = true;
        }
    }

    public void ToggleOptOut()
    {
        if (optInit)
        {
            AudioController.Play("button");
            optOut = !optOut;
            Analytics.gua.analyticsDisabled = optOut;
            Debug.Log("Google Analytics Opt Out: " + optOut);
            PlayerPrefs.SetInt("OptOut", (optOut) ? 1 : 0);
            PlayerPrefs.Save();
        }
        else
            optInit = true;
    }

    public void ClickDanielTwitter()
    {
        AudioController.Play("button");
        GoogleAnalytics.SendEvent("poem_twitter");
        Application.OpenURL("http://twitter.com/poemdexter");
    }

    public void ClickDanielWebsite()
    {
        AudioController.Play("button");
        GoogleAnalytics.SendEvent("poem_website");
        Application.OpenURL("http://poemdexter.com/");
    }

    public void ClickRobertTwitter()
    {
        AudioController.Play("button");
        GoogleAnalytics.SendEvent("xezton_twitter");
        Application.OpenURL("http://twitter.com/xezton");
    }

    public void ClickRobertWebsite()
    {
        AudioController.Play("button");
        GoogleAnalytics.SendEvent("xezton_website");
        Application.OpenURL("http://seatonweb.com/");
    }
}