﻿using UnityEngine;
using System.Collections;
using poemdexter.RFS;

public class TouchHandler : MonoBehaviour
{
    private TilesManager tileManager;
    public float deadZone;
    public GameObject trailPrefab;
    private GameObject trail;

    void Start()
    {
        tileManager = GetComponent<TilesManager>();
        trail = Instantiate(trailPrefab) as GameObject;
        trail.SetActive(false);
    }

    void Update()
    {
        // Look for all fingers
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // -- Drag
            if (touch.phase == TouchPhase.Began)
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(touch.position);
                position.z = -0.3f; // Make sure the trail is visible
                trail.transform.position = position;
                trail.SetActive(true);

            }
            else if (touch.phase == TouchPhase.Moved)
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(touch.position);
                position.z = -0.3f; // Make sure the trail is visible
                trail.transform.position = position;

            }
            else if (touch.phase == TouchPhase.Ended)
            {
                StartCoroutine(WaitForTrail());
            }
        }
    }

    IEnumerator WaitForTrail()
    {
        for (float timer = .5f; timer >= 0; timer -= Time.deltaTime)
            yield return 0;
        trail.SetActive(false);
    }

    void OnPress(bool isDown)
    {
        if (!isDown)
            CheckForSwipe(UICamera.currentTouch.totalDelta);
    }

    private void CheckForSwipe(Vector2 delta)
    {
        if (IsOutsideDeadZone(delta))
        {
            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
            { // horizontal swipe
                if (delta.x < 0)
                {
                    tileManager.HandleMove(Utils.LEFT);
                }
                else
                {
                    tileManager.HandleMove(Utils.RIGHT);
                }
            }
            else
            { // vertical swipe
                if (delta.y < 0)
                {
                    tileManager.HandleMove(Utils.DOWN);
                }
                else
                {
                    tileManager.HandleMove(Utils.UP);
                }
            }
        }
    }

    private bool IsOutsideDeadZone(Vector2 delta)
    {
        return (delta.x > deadZone || delta.x < -deadZone || delta.y > deadZone || delta.y < -deadZone) ? true : false;
    }
}